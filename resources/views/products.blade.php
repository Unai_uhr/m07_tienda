@extends('layouts.app')

@section('content')

    @if(session('error'))

        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        <div class="alert alert-error">
            {{ session('error') }}
        </div>

    @endif

    <style>
        .card-body{
            display: flex;
            flex-wrap: wrap;
            width: 100%;
            margin: auto;
        }
        .product-card{
            border: 2px solid grey;
            border-radius: 20px 20px ;
            background: white;
            width: 20%;
            margin: 10px;
            padding: 10px;
            display: flex;
            flex-direction: column;
            align-items: end;
            justify-content: space-between;
        }
        .product-card>img{
            width: 100%;
            height: 75%;
        }
        .product-card>h2{
            color: black;
            font-size: 20px;
            font-weight: bold;
        }
        .product-card>p{
            color: darkgrey;
            font-style: italic;
        }
        .btn{
            text-decoration: none;
            text-align: center;
            color: white;
            font-style: normal;
            border: 1px solid black;
            border-radius: 5px 5px;
            padding: 5px;
            background: cornflowerblue;
        }
        .btn-warning{
            background: firebrick;
        }
        option[value='a']{
            color: darkgrey;
            font-style: oblique;
        }
    </style>

    <div class="container">
        <div class="card-header">
            <form method="get" action="/products">
                <select name="filterPrice" id="filterPrice">
                    <option  value="a" hidden selected>Seleccione el rango de precios</option>
                    <option value="0">Todos los productos</option>
                    <option value="1">Menos de 5€</option>
                    <option value="2">Entre 5€ y 10€</option>
                    <option value="3">Más de 10€</option>
                </select>
                <input class="btn" value="Filtrar" type="submit"/>
            </form>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <!--<div class="card-header">{{ __('Products') }}</div>-->
                    <div class="card-body">
                        @foreach ($products as $product)

                            <div class="product-card">
                                <div>
                                    <img src="data:image/jpeg;base64,{!! stream_get_contents($product->image) !!}"/>
                                    <h2>{{$product->name}}</h2>
                                    <p>{{$product->price}}€</p>
                                </div>
                                <div>
                                    <p class="btn-holder"><a href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-warning btn-block text-center" role="button">añadir</a> </p>
                                </div>
                            </div>



                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        {{$products->links()}}
    </div>
@endsection
