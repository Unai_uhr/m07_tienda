<?php

return[
    'Welcome' => 'Bienvenido',
    'Register'=> 'Registro',
    'language'=> 'Idioma',
    'Name' => 'Nombre',
    'Surname'=> 'Apellidos',
    'Dni' => 'DNI',
    'Email' => 'Correo electronico',
    'Password' => 'Contraseña',
    'ConfirmPassword' => 'Confirme contraseña',
    'AlreadyRegistered?'=> '¿Ya estas registrado?',
    'Register'=> 'Registrar',
];

?>
