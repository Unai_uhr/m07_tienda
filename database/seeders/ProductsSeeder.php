<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name'=> 'Cuadernos de espiral',
            'price'=> 9.99,
            'stock'=> 15,
            'description'=>'description product',
            'image'=>base64_encode(file_get_contents('/home/itb/tienda/cuadernos.jpg')),
        ]);
        DB::table('products')->insert([
            'name'=> 'Agenda Escolar',
            'price'=> 13.5,
            'stock'=> 15,
            'description'=>'description product 2',
            'image'=>base64_encode(file_get_contents('/home/itb/tienda/agenda.jpg')),
        ]);
        DB::table('products')->insert([
            'name'=> 'Acuarelas',
            'price'=> 19.99,
            'stock'=> 15,
            'description'=>'description product 2',
            'image'=>base64_encode(file_get_contents('/home/itb/tienda/acuarelas.jpg')),
        ]);
        DB::table('products')->insert([
            'name'=> 'Juego de reglas',
            'price'=> 17.99,
            'stock'=> 15,
            'description'=>'description product 2',
            'image'=>base64_encode(file_get_contents('/home/itb/tienda/juegoReglas.jpg')),
        ]);
    }
}
