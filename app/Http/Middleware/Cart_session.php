<?php

namespace App\Http\Middleware;

use App\Models\Cart;
use App\Models\Cart_items;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Cart_session
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user= Auth::user()->id;

        $cart_db=Cart::where("user_id",$user)->first();
        //dd($cart_db->id);

        //$cart=Cart_items::where([['cart_id',$cart_db->id]]);
        $cart=DB::select(DB::raw("select * from cart_items where cart_id = $cart_db->id"));
        //dd($cart);
        $cartsesion=[];
        foreach ($cart as $item){
            //dd($item);
            $productName=DB::select(DB::raw("select name, price,image from products where id = $item->product_id"));
            //dd($productName[0]->price);
            Log::error("---------------------->> " . $item->product_id);
            $cartsesion=[
                $item->product_id => [
                    "name" => $productName[0]->name,
                    "amount" => $item->amount,
                    "price" => $productName[0]->price,
                    "image" => $productName[0]->image
                ],
            ];
            //dd($cart);
        }
       session()->put('cart', $cartsesion);
        //dd($cartsesion);
        return $next($request);
    }
}
