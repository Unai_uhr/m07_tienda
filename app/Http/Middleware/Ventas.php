<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Product;


class Ventas
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $compraCorrecta=true;
        $cart = session()->get('cart');
        foreach ($cart as $idProduct => $item){
            //dd($id);
            $cantidadPedida=$item['amount'];
            //$stock=DB::select(DB::raw("select stock from products where id=".$idProduct))->first();
            $stock=Product::where([['id',$idProduct]])->first();
            //dd($cantidadPedida." < ".$stock['stock']." -> ".$cantidadPedida<$stock['stock']);
            if($cantidadPedida>$stock['stock']){
                $compraCorrecta=false;
            }
        }

        //dd($compraCorrecta);
        if($compraCorrecta){
            //dd("HAY STOCK");
            //session()->flash('success', 'Compra Realizada');
            return $next($request);
        }else{
            $resul=['stock'=>false];
            //$request.cookie('stock',false);
            //return $next($request);
            //dd(" NO HAY STOCK");
            session()->flash('error', 'Stock Insuficiente');
            return redirect('cart');
            //session()->flash('warning', 'Stock Insuficiente');
            //return redirect('products');
        }

    }
}
