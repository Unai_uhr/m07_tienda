<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Models\Product;
use App\Models\Cart;
use App\Models\Cart_items;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Request $request)
    {

        $value = empty($request->input('price')) ? '0' : $request->input('price');
        $filtro = empty($request->input('filterPrice')) ? '0' : $request->input('filterPrice');
        switch ($filtro){
            case 0:
                $all = DB::select(DB::raw("select * from products"));
                break;
            case 1:
                $all = DB::select(DB::raw("select * from products where price <5"));
                break;
            case 2:
                $all = DB::select(DB::raw("select * from products where price >=5 and price <=10"));
                break;
            case 3:
                $all = DB::select(DB::raw("select * from products where price > 10"));
                break;
        }

        //$all = DB::select(DB::raw("select * from products where price > $value"));
        // PARA DEBUGAR  {{dd($variable}}
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($all);
        $perPage = 4;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $products = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);
        $products->setPath($request->url());

        return view('products')->with('products',$products);

    }


    public function addToCart($id){
        $product = Product::find($id);

        Log::error('error');
        Log::warning('warning');
        Log::info('info');

        Log::channel('buy')->info('Ejemplo '. $id);

        Log::channel('suspicious')->info('Ejemplo de MI LOG SUSPICIOUS '. $id);

        $user= Auth::user()->id;
        $cart_db=Cart::where("user_id",$user)->first();
        if (empty($cart_db)){

            $cart_db= new Cart();
            $cart_db->user_id = $user;
            $cart_db->save();
        }

        if(!$product) {
            abort(404);
        }
        $cart = session()->get('cart');

        if(!$cart) {
            $cart = [
                $id => [
                    "name" => $product->name,
                    "amount" => 1,
                    "price" => $product->price,
                    "image" => $product->image
                ]
            ];
            $cart_items = new Cart_items();
            $cart_items->cart_id=$cart_db->id;
            $cart_items->product_id=$id;
            $cart_items->amount =1;
            $cart_items->save();

            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }
        //Si existe el carrito y el producto esta ya en el carrito
        if(isset($cart[$id])) {
            $cart[$id]['amount']++;
            //dd($cart[$id]);
     //       dd(Cart_items::where([['cart_id',$cart_db->id],['product_id',$id]])->first());
            //$nuevaCantidad=$cart[$id]['amount']+1;
            $cart_items = Cart_items::where([['cart_id',$cart_db->id],['product_id',$id]])->first();

          //  dd($cart_db->id);
            $cart_items->amount=$cart[$id]['amount'];
            $cart_items->update();
           // $cart_items->save();

            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        $cart[$id] = [
            "name" => $product->name,
            "amount" => 1,
            "price" => $product->price,
            "image" => $product->imageage
        ];
        $cart_item = new Cart_items();
        $cart_item->cart_id=$cart_db->id;
        $cart_item->product_id=$id;
        $cart_item->amount=1;
        $cart_item->save();

        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function cart()
    {
        return view('cart');
    }

    public function update(Request $request){
        if($request->id && $request->amount){
            $cart = session()->get('cart');
            $cart[$request->id]["amount"] = $request->amount;

            $user = Auth::user()->id;
            $cart_db = Cart::where('user_id',$user)->first();

            $cart_item = Cart_items::where([['cart_id',$cart_db->id],['product_id',$request->id]])->first();
            $cart_item->amount=$request->amount;
            $cart_item->update();

            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request){
        if($request->id) {
            $cart = session()->get('cart');
            //{{dd($cart[$request->id])}}

            if(isset($cart[$request->id])) {
                unset($cart[$request->id]);
                //if($request->id && $request->amount) {
                    //$cart[$request->id]["amount"] = $request->amount;

                    $user = Auth::user()->id;
                    $cart_db = Cart::where('user_id',$user)->first();

                    $cart_item = Cart_items::where([['cart_id',$cart_db->id],['product_id',$request->id]])->first();
                    $cart_item->delete();
                    session()->put('cart', $cart);
               // }
            }
            session()->flash('success', 'Product removed successfully');
        }
    }

    public function ventas(Request $request){
        $cart = session()->get('cart');
        //dd($cart);

        foreach ($cart as $item){

            $nombre=$item['name'];
            $cantidad=$item['amount'];
            //ACTUALIZAMOS STOCK
            $stockUpdate = DB::select(DB::raw("select * from products where name = '$nombre'"));
            $stockResultante=$stockUpdate[0]->stock-$cantidad;
            $stockUpdate = DB::update(DB::raw("update products set stock=$stockResultante where name = '$nombre'"));

        }
        //QUITAMOS CARRITO DE BD
        $user= Auth::user()->id;
        Cart::where('user_id',$user)->first()->delete();

        //VACIAMOS CARRITO

        unset($cart);
        session()->put('cart', '');









        return redirect()->back()->with('success', 'COMPRA REALIZADA');
    }
}

